from app import app
from flask import render_template
from models import *

PER_PAGE = 10

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/contacts')
def contact():
    return render_template('contacts.html')


@app.route('/projects')
def projects():
    return render_template('projects.html')


@app.route('/proposal')
def proposal():
    return render_template('proposal.html')


@app.route('/ekspirience')
def ekspirience():
    return render_template('ekspirience.html')


@app.route('/courses')
def courses():
    article = Courses().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('courses.html')


@app.route('/conference')
def conference():
    return render_template('conference.html')


@app.route('/books')
def books():
    return render_template('books.html')


@app.route('/articles')
def articles():
    return render_template('articles.html')


@app.route('/business_project')
def business_project():
    return render_template('business_project.html')


@app.route('/work_with_students')
def work_with_students():
    return render_template('work_with_students.html')
