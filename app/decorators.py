from functools import wraps
from flask import redirect
from google.appengine.api import users
from models import *
from app import app


def only_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if users.get_current_user():
            admins = ["sink2385@gmail.com", "maxim.vinnik@gmail.com", "savchenko.eo@gmail.com"]
            for admin in Admin.all():
                admins.append(admin.email)
            if users.get_current_user().email().lower() in admins or app.debug:
                pass
            else:
                return redirect(users.create_logout_url('/'))
        else:
            return redirect(users.create_login_url('/admin'))
        return f(*args, **kwargs)
    return decorated_function