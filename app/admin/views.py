from flask import Blueprint, render_template, flash, redirect, url_for, jsonify, request, abort
from app.models import *
from app.decorators import *
from forms import *
from datetime import datetime
import json
mod = Blueprint('admin', __name__, url_prefix='/admin', template_folder='templates')


@mod.route('/', methods=['GET', 'POST'])
@only_admin
def admin():
    return render_template('admin.html', admins=Admin.all(), admin_form=AdminForm())


@mod.route('/add_admin', methods=['GET', 'POST'])
@only_admin
def add_admin():
    admin_form = AdminForm(request.form)
    key = ''
    if admin_form.validate():
        admin = Admin(email=admin_form.email.data)
        admin.put()
        key = admin.key()
    return json.dumps({'valid': admin_form.validate_on_submit(), 'key': str(key), 'email': admin_form.email.data})


@mod.route('/add_edit_about_me', methods=['GET', 'POST'])
@only_admin
def add_edit_about_me():
    return render_template('add_edit_about_me.html', photos=PhotoAboutMe().all(), article_about_me="")


@mod.route('/edit_education', methods=['GET', 'POST'])
@only_admin
def edit_education():
    article = Education().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_education")


@mod.route('/submit_education', methods=['GET', 'POST'])
@only_admin
def submit_education():
    article = Education.all().get()
    data = json.loads(request.data)
    if not article:
        article = Education(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_work', methods=['GET', 'POST'])
@only_admin
def edit_work():
    article = Work().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_work")


@mod.route('/submit_work', methods=['GET', 'POST'])
@only_admin
def submit_work():
    article = Work.all().get()
    data = json.loads(request.data)
    if not article:
        article = Work(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_project', methods=['GET', 'POST'])
@only_admin
def edit_project():
    article = Project().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_project")


@mod.route('/submit_project', methods=['GET', 'POST'])
@only_admin
def submit_project():
    article = Project.all().get()
    data = json.loads(request.data)
    if not article:
        article = Project(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_my_project', methods=['GET', 'POST'])
@only_admin
def edit_my_project():
    article = MyProject().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_my_project")


@mod.route('/submit_my_project', methods=['GET', 'POST'])
@only_admin
def submit_my_project():
    article = MyProject.all().get()
    data = json.loads(request.data)
    if not article:
        article = MyProject(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_traineeship', methods=['GET', 'POST'])
@only_admin
def edit_traineeship():
    article = Traineeship().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_traineeship")


@mod.route('/submit_traineeship', methods=['GET', 'POST'])
@only_admin
def submit_traineeship():
    article = Traineeship.all().get()
    data = json.loads(request.data)
    if not article:
        article = Traineeship(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_courses', methods=['GET', 'POST'])
@only_admin
def edit_courses():
    article = Courses().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_courses")


@mod.route('/submit_courses', methods=['GET', 'POST'])
@only_admin
def submit_courses():
    article = Courses.all().get()
    data = json.loads(request.data)
    if not article:
        article = Courses(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_conference', methods=['GET', 'POST'])
@only_admin
def edit_conference():
    article = Conference().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('edit_article.html', article=article, link="submit_conference")


@mod.route('/submit_conference', methods=['GET', 'POST'])
@only_admin
def submit_conference():
    article = Conference.all().get()
    data = json.loads(request.data)
    if not article:
        article = Conference(body=data["article"])
        article.put()
    else:
        article.body = data["article"]
        article.put()
    return json.dumps({'valid': True})


@mod.route('/edit_contact', methods=['GET', 'POST'])
@only_admin
def edit_contacts():
    return render_template('edit_contact.html', emails=ContactsEmail.all(), email_form=EmailForm(),
                           contacts=ContactSocialForm(), networks=ContatctsSocial.all(),
                           contact_form=AllContactForm(),
                           allcontacts=AllContacts.all())


@mod.route('/add_photo', methods=['GET', 'POST'])
@only_admin
def add_photo():
    photo = PhotoAboutMe()
    if request.files['photo']:
        photo.photo = db.Blob(request.files['photo'].read())
        photo.put()
        id = photo.key().id()
        return json.dumps({'valid': True, 'id': id, 'key': str(photo.key())})
    return json.dumps({'valid': False})


@mod.route('/add_email', methods=['GET', 'POST'])
@only_admin
def add_email():
    email_form = EmailForm(request.form)
    key = ''
    if email_form.validate():
        Email = ContactsEmail(email=email_form.email.data)
        Email.put()
        key = Email.key()
    return json.dumps({'valid': email_form.validate_on_submit(), 'key': str(key), 'email': email_form.email.data})


@mod.route('/add_network', methods=['GET', 'POST'])
@only_admin
def add_network():
    networkform = ContactSocialForm(request.form)
    key = ''
    nameerror = False
    urlerror = False
    logo = False
    if networkform.validate():
        Network = ContatctsSocial(name=networkform.name.data, url=networkform.url.data)
        if request.files['logo']:
            Network.logo = db.Blob(request.files['logo'].read())
            logo = True
        Network.put()
        key = Network.key()
    else:
        if networkform.name.errors:
            nameerror = True
        if networkform.url.errors:
            urlerror = True
    return json.dumps({'valid': networkform.validate_on_submit(),
                       'key': str(key),
                       'url': networkform.url.data,
                       'name': networkform.name.data,
                       'name-error': nameerror,
                       'url-error': urlerror,
                       'logo': logo})


@mod.route('/add_column', methods=['GET', 'POST'])
@only_admin
def add_column():
    columnform = AllContactForm(request.form)
    key = ''
    columnerror = False
    valueerror = False
    if columnform.validate():
        Column = AllContacts(nameColumn=columnform.column.data, valueColumn=columnform.value.data)
        Column.put()
        key = Column.key()
    else:
        if columnform.column.errors:
            columnerror = True
        if columnform.value.errors:
            valueerror = True
    return json.dumps({'valid': columnform.validate_on_submit(),
                       'key': str(key),
                       'column': columnform.column.data,
                       'value': columnform.value.data,
                       'column-error': columnerror,
                       'value-error': valueerror})


@mod.route('/submit_articles_about_me', methods=['GET', 'POST'])
@only_admin
def submit_articles_about_me():
    article = ArticleAboutMe.all().get()
    if not article:
        article = ArticleAboutMe(body=request.args.get('article'))
        article.put()
    else:
        article.body = request.args.get('article')
        article.put()
    return json.dumps({'valid': True})


@mod.route('/delete_photo/<string:key>', methods=['GET', 'POST'])
@only_admin
def delete_photo(key):
    db.delete(key)
    return json.dumps({})


@mod.route('/delete_by_key/<string:key>', methods=['GET', 'POST'])
@only_admin
def delete_by_key(key):
    db.delete(key)
    return json.dumps({})


# Blog
@mod.route('/blog/group/<int:group_id>', methods=['POST'])
@only_admin
def blog_group_get(group_id):
    return jsonify(group=GroupsOfBlogs.get(group_id))


@mod.route('/blog/group/all', methods=['POST'])
@only_admin
def blog_group_get_all():
    return jsonify(groups=GroupsOfBlogs.get().all())


@mod.route('/blog/group/add', methods=['POST'])
@only_admin
def blog_group_add():
    data = json.loads(request.data)

    try:
        group = GroupsOfBlogs(name=data["name"], color=data["color"])
    except (ValueError, KeyError, TypeError):
        return abort(400)

    group.put()

    return json.dumps({"key": group.key().id()})


@mod.route('/blog/group/<int:group_id>/edit', methods=['POST'])
@only_admin
def blog_group_edit(group_id):
    data = json.loads(request.data)
    group = GroupsOfBlogs.get_by_id(group_id)

    try:
        group.name = data["name"]
        group.color = data["color"]
    except (ValueError, KeyError, TypeError):
        return abort(400)

    group.put()

    return json.dumps({})


@mod.route('/blog/group/<int:group_id>/delete', methods=['POST'])
@only_admin
def blog_group_delete(group_id):
    blogs_by_group = Blog.all().filter('group = ', GroupsOfBlogs.get_by_id(long(group_id)))

    if blogs_by_group.count():
        return json.dumps({"msg": "Can't delete this group, because have blogs with that group"})

    group = GroupsOfBlogs.get_by_id(group_id)
    group.delete()

    return json.dumps({})


# page: admin/blog
@mod.route('/blog', methods=['GET'])
@only_admin
def blog():
    return render_template("admin_blog.html", blogs=db.Query(Blog).order("-date"), groups=GroupsOfBlogs.all())


@mod.route('/blog/<int:blog_id>', methods=['POST'])
@only_admin
def blog_get(blog_id):
    return jsonify(blog=Blog.get(blog_id))


@mod.route('/blog/<int:blog_id>/amount/<int:amount_blogs>')
@only_admin
def blog_get_certain_amount(blog_id, amount_blogs):
    return jsonify(blogs=Blog.all().order("-data").ancestor(blog_id).fetch(limit=amount_blogs))


@mod.route('/blog/add', methods=['POST'])
@only_admin
def blog_add():
    data = json.loads(request.data)

    Blog(name=data["name"], content=data["content"], group=GroupsOfBlogs.get_by_id(long(data["group"]))).put()

    return json.dumps({})


@mod.route('blog/<int:blog_id>/edit', methods=['POST'])
@only_admin
def blog_edit(blog_id):
    data = json.loads(request.data)
    blog = Blog.get_by_id(blog_id)

    try:
        blog.name = data["name"]
        blog.content = data["content"]
        blog.group = GroupsOfBlogs.get_by_id(long(data["group"]))

        blog.put()
    except (ValueError, KeyError, TypeError):
        return abort(400)

    return json.dumps({})

@mod.route('blog/<int:blog_id>/delete', methods=['POST'])
@only_admin
def blog_delete(blog_id):
    Blog.get_by_id(blog_id).delete()
    return json.dumps({})


@mod.route('/font', methods=['GET'])
@only_admin
def font():
    return render_template('edit_font.html', fonts=Font.all(), font_form=FontForm(),
                           font_selected=Settings.all().get())


@mod.route('/font/add', methods=['GET', 'POST'])
@only_admin
def font_add():
    font_form = FontForm(request.form)
    key = ''
    if font_form.validate():
        font = Font(name=font_form.name.data, link=font_form.link.data, css=font_form.css.data)
        font.put()
        key = font.key()
    return json.dumps({'valid': font_form.validate_on_submit(), 'key': str(key), 'name': font_form.name.data,
                       'link': font_form.link.data, 'css': font_form.css.data})


@mod.route('/font/<int:font_id>', methods=['GET', 'POST'])
@only_admin
def font_select(font_id):
    settings = Settings.all().get()
    if not settings:
        settings = Settings(font=Font.get_by_id(font_id))
        settings.put()
    else:
        settings.font = Font.get_by_id(font_id)
        settings.put()
    return json.dumps({})

