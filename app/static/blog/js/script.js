$(document).ready( function() {
    $('.input-colorpicker').colorpicker({
        format: "rgba"
    });

    $('textarea[name=content]').ckeditor();

    /* Groups */
    $("#form-group-edit select").change(function() {
        $("#form-group-edit input[name='name']").val($("#form-group-edit option:selected").text());
        $("#form-group-edit input[name='color']").val($("#form-group-edit option:selected").attr("color"));
        $("#form-group-edit").attr("group-id", $("#form-group-edit option:selected").val());

        if ($("#form-group-edit select > option").length) {
            $("#form-group-edit button").removeAttr("disabled");
        } else {
            $("#form-group-edit button").attr("disabled", "");
        }
    }).change();

    $("#form-group-add").fastform("success", function(data) {
        var $form = $("#form-group-add");
        var $newOption =
            $("<option></option>")
                .val(data["key"])
                .attr("color", $form.find("input[name=color]").val())
                .html($form.find("input[name=name]").val());

        $("select[select-group]").append($newOption);

        $("select[select-group]").change();
    });

    $("#form-group-edit").fastform("data", function() {
        var data = {};

        data["name"] = $("#form-group-edit input[name='name']").val();
        data["color"] = $("#form-group-edit input[name='color']").val();

        return data;
    }, "edit");

    $("#form-group-edit").fastform("data", function() {
        return {};
    }, "delete");

    $("#form-group-edit").fastform("url", function() {
        return this.url.replace("0", this.form.attr("group-id"));
    });

    $("#form-group-edit").fastform("success", function() {
        var $form = $("#form-group-edit"),
            $option = $("select[select-group] option:selected");

        $option.attr("color", $form.find("input[name=color]").val());
        $option.text($form.find("input[name=name]").val());
    }, "edit");

    $("#form-group-edit").fastform("success", function(data) {
        if (!data["msg"]) {
            $("select[select-group] option:selected").remove();

            $("#form-group-edit select").change();
            if (!$("#form-group-edit select > option").length) {
                $("#form-group-edit button").attr("disabled", "");
            }
        }
    }, "delete");

    $("#form-blog-add").fastform("success", function() {
        location.reload();
    });

    $("#form-blog-edit").fastform("data", function() {
        return {};
    }, "delete");

    $("#form-blog-edit").fastform("success", function() {
        location.reload();
    }, "delete");
    /* Groups: END */
});